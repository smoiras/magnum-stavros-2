# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import requests
import json

from oslo_config import cfg
from magnum.common import exception
from magnum.i18n import _

CONF = cfg.CONF

class OidcApplication:
    """OpenID Connect support for kubernetes clusters at CERN

    This class creates and deletes applications through the API
    """

    def __init__(self):

        self.applicationURL = CONF.oidc.applicationURL
        self.registerURL = CONF.oidc.registerURL
        self.getTokenURL = CONF.oidc.getTokenURL
        self.identityURL = CONF.oidc.identityURL
        self.payload = {}
        self.headers = {}
        self.client_id = CONF.oidc.client_id
        self.secret = CONF.oidc.client_secret

        # Get bearer token
        self.payload = {
            "grant_type": "client_credentials",
            "audience": CONF.oidc.audience,
            "client_id": self.client_id,
            "client_secret": self.secret
        }

        try:
            response = requests.post(self.getTokenURL, data=self.payload)
            response.raise_for_status()
        except requests.exceptions.RequestException as err:
            raise RuntimeError(err)

        responsedictionary = json.loads(response.text)

        # This is our authorization token
        self.token = responsedictionary["access_token"]

        self.headers = {
                        "accept": "*/*",
                        "Authorization": "Bearer " + self.token,
                        "Content-Type": "application/json"
                        }

    def __getUserId(self, username):

        try:
            response = requests.get(self.identityURL + "?filter=upn:" + username + "&field=id", headers=self.headers)
            response.raise_for_status()

            responsedictionary = json.loads(response.text)

            # This is in case the username is invalid
            if not responsedictionary["data"]:
                raise exception.OIDCfailed(reason='API returned nothing, is the username' + username + ' valid?')

            # This is our user's id
            userId = responsedictionary["data"][0]["id"]

            # Check if the length of the id is correct
            if not len(userId) == 36:
                raise exception.OIDCfailed(reason='API returned a userID ' + userId + ' with a length that exceeds 36 characters')

            return userId

        except Exception as err:
            raise exception.OIDCfailed(reason=err)

    def __getApplicationId(self, applicationIdentifier):

        try:
            response = requests.get(self.applicationURL + "?filter=applicationIdentifier:" + applicationIdentifier + "&field=id", headers=self.headers)
            response.raise_for_status()

            responsedictionary = json.loads(response.text)

            # This is in case the applicationIdentifier is invalid
            if not responsedictionary["data"]:
                raise exception.OIDCfailed(reason='API returned nothing, is the applicationIdentifier ' + applicationIdentifier + ' valid?')

            # This is our application's id
            applicationId = responsedictionary["data"][0]["id"]

            # Check if the length of the id is correct
            if not len(applicationId) == 36:
                raise exception.OIDCfailed(reason='API returned an applicationID ' + applicationId + ' with a length that exceeds 36 characters')

            return applicationId

        except Exception as err:
            raise exception.OIDCfailed(reason=err)

    def __registerSSO(self, applicationId):

        self.payload = {
            "publicClient":True,
            "implicitFlowEnabled":True,
            "serviceAccountsEnabled":False,
            "redirectUris":["urn:ietf:wg:oauth:2.0:oob", "http://localhost:8000"],
            "description":"Created by openstack-magnum"
        }

        try:
            # f0000000-0000-0000-0000-000000000051 corresponds to the authentication protocol (OIDC in our case)
            response = requests.post(self.registerURL + "/" + applicationId + "/f0000000-0000-0000-0000-000000000051", data=json.dumps(self.payload), headers=self.headers)
            response.raise_for_status()
        except Exception as err:
            raise exception.OIDCfailed(reason=err)

        return

    def create(self, applicationIdentifier, displayName, description, managerId, ownerUsername):

        # Get user's id to use it as 'ownerId'
        userId = self.__getUserId(username=ownerUsername)

        self.payload = {
            "applicationIdentifier": applicationIdentifier,
            "displayName": displayName,
            "description": description,
            "managerId": managerId,
            "ownerId": userId
        }

        try:
            response = requests.post(self.applicationURL, data=json.dumps(self.payload), headers=self.headers)
            response.raise_for_status()
        except Exception as err:
            raise exception.OIDCfailed(reason=err)

        # Add a new single sign-on registration for our application
        applicationId = self.__getApplicationId(applicationIdentifier)  # This might be unnecessary because the 'application id' may already be in the response.text
        self.__registerSSO(applicationId)

        return

    def delete(self, applicationIdentifier):

        # Get application id first in order to delete the application
        applicationId = self.__getApplicationId(applicationIdentifier)

        try:
            response = requests.delete(self.applicationURL + "/" + applicationId, headers=self.headers)
            response.raise_for_status()
        except Exception as err:
            raise exception.OIDCfailed(reason=err)

        return
