set +x
. /etc/sysconfig/heat-params
set -x

$ssh_cmd mkdir -p /etc/kubernetes/
$ssh_cmd cp /etc/pki/tls/certs/ca-bundle.crt /etc/kubernetes/ca-bundle.crt

if [ -n "${TRUST_ID}" ]; then
    KUBE_OS_CLOUD_CONFIG=/etc/kubernetes/cloud-config

    # Generate a the configuration for Kubernetes services
    # to talk to OpenStack Neutron and Cinder
    cat > ${KUBE_OS_CLOUD_CONFIG} <<EOF
[Global]
auth-url=$AUTH_URL
user-id=$TRUSTEE_USER_ID
password=$TRUSTEE_PASSWORD
trust-id=$TRUST_ID
ca-file=/etc/kubernetes/ca-bundle.crt
[LoadBalancer]
use-octavia=$OCTAVIA_ENABLED
subnet-id=$CLUSTER_SUBNET
floating-network-id=$EXTERNAL_NETWORK_ID
create-monitor=yes
monitor-delay=1m
monitor-timeout=30s
monitor-max-retries=3
[BlockStorage]
bs-version=v2
EOF

    # Provide optional region parameter if it's set.
    if [ -n "${REGION_NAME}" ]; then
        sed -i '/ca-file/a region='${REGION_NAME}'' $KUBE_OS_CLOUD_CONFIG
    fi

    # backwards compatibility, some apps may expect this file from previous magnum versions.
    $ssh_cmd cp ${KUBE_OS_CLOUD_CONFIG} /etc/kubernetes/kube_openstack_config

    # occm specific config with additional options.
    cat > ${KUBE_OS_CLOUD_CONFIG}-occm <<EOF
[Global]
auth-url=$AUTH_URL
user-id=$TRUSTEE_USER_ID
password=$TRUSTEE_PASSWORD
trust-id=$TRUST_ID
ca-file=/etc/kubernetes/ca-bundle.crt
[LoadBalancer]
use-octavia=$OCTAVIA_ENABLED
subnet-id=$CLUSTER_SUBNET
floating-network-id=$EXTERNAL_NETWORK_ID
create-monitor=yes
monitor-delay=1m
monitor-timeout=30s
monitor-max-retries=3
network-id=798d00f3-2af9-48a0-a7c3-a26d909a2d64
lb-provider=opencontrail
internal-lb=True
cascade-delete=False
batch-member-update=False
[BlockStorage]
bs-version=v2
[Networking]
internal-network-name=$CLUSTER_NETWORK_NAME
EOF

    # Provide optional region parameter if it's set.
    if [ -n "${REGION_NAME}" ]; then
        sed -i '/ca-file/a region='${REGION_NAME}'' ${KUBE_OS_CLOUD_CONFIG}-occm
    fi
fi
