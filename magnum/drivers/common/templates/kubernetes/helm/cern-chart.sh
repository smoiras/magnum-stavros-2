#!/bin/bash

. /etc/sysconfig/heat-params

set -ex

step="cern-chart"
printf "Starting to run ${step}\n"

### Configure installation dependencies
###############################################################################
if [ "$(echo ${MONITORING_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ] && \
   [ "$(echo ${METRICS_PRODUCER})" != "" ]; then
    CERN_CENTRAL_MONITORING="true"
else
    CERN_CENTRAL_MONITORING="false"
fi
# CERN_CENTRAL_MONITORING is not a magnum value, need to write it in heat-params
echo "CERN_CENTRAL_MONITORING=\"${CERN_CENTRAL_MONITORING}\"" >> /etc/sysconfig/heat-params

if [ "$(echo ${LOGGING_INSTALLER} | tr '[:upper:]' '[:lower:]')" = "helm" ] && \
   [ ! -z "${LOGGING_PRODUCER}" ]; then
    CERN_CENTRAL_LOGGING="true"
else
    CERN_CENTRAL_LOGGING="false"
fi
# CERN_CENTRAL_LOGGING is not a magnum value, need to write it in heat-params
echo "CERN_CENTRAL_LOGGING=\"${CERN_CENTRAL_LOGGING}\"" >> /etc/sysconfig/heat-params

NVIDIA_GPU_VALUES=""
if [ "$(echo ${NVIDIA_GPU_TAG})" != "" ]; then
    NVIDIA_GPU_VALUES="
      plugin:
        image:
          tag: ${NVIDIA_GPU_TAG}
      installer:
        image:
          tag: ${NVIDIA_GPU_TAG}
"
fi

### Configuration
###############################################################################
CHART_NAME="cern-magnum"
_cern_chart_values_file="/srv/magnum/kubernetes/cern_chart_values.yaml"
if [ "$(echo ${CERN_CHART_VALUES})" != "" ]; then
    # decode base64 and indent by 4 spaces to write the ConfigMap below
    echo ${CERN_CHART_VALUES} | base64 -d | sed 's/^/    /g'> ${_cern_chart_values_file}
    # backwards compatible with labels happens later with envsubst
    # envsubst does not work with shell variables, it works only with environment variables.
    while read line
    do
        heat_param_name=$(echo $line | cut -d"=" -f1)
        heat_param_value=$(echo $line | cut -d"=" -f2 | sed 's/"//g')
        sed -i "s#\${${heat_param_name}}#${heat_param_value}#g" ${_cern_chart_values_file}
    done < /etc/sysconfig/heat-params
else
    cat <<EOF > ${_cern_chart_values_file}
    eosxd:
      enabled: ${EOS_ENABLED}
    nvidia-gpu:
      enabled: ${NVIDIA_GPU_ENABLED}
${NVIDIA_GPU_VALUES}
    fluentd:
      enabled: ${CERN_CENTRAL_LOGGING}
      output:
        producer: ${LOGGING_PRODUCER}
        endpoint: ${LOGGING_HTTP_DESTINATION}
        includeInternal: ${LOGGING_INCLUDE_INTERNAL}
      containerRuntime: ${CONTAINER_RUNTIME}
    landb-sync:
      enabled: ${LANDB_SYNC_ENABLED}
    prometheus-cern:
      enabled: ${CERN_CENTRAL_MONITORING}
    ceph-csi-cephfs:
      enabled: ${CEPHFS_CSI_ENABLED}
    openstack-cinder-csi:
      enabled: ${CINDER_CSI_ENABLED}
    openstack-manila-csi:
      enabled: ${MANILA_CSI_ENABLED}
    base:
      enabled: ${CERN_ENABLED}
EOF
fi

if [ "$(echo ${CERN_CHART_ENABLED} | tr '[:upper:]' '[:lower:]')" = "true" ]; then
    HELM_MODULE_CONFIG_FILE="/srv/magnum/kubernetes/helm/${CHART_NAME}.yaml"
    [ -f ${HELM_MODULE_CONFIG_FILE} ] || {
        echo "Writing File: ${HELM_MODULE_CONFIG_FILE}"
        mkdir -p $(dirname ${HELM_MODULE_CONFIG_FILE})
        cat << EOF > ${HELM_MODULE_CONFIG_FILE}
---
kind: ConfigMap
apiVersion: v1
metadata:
  name: ${CHART_NAME}-config
  namespace: magnum-tiller
  labels:
    app: helm
data:
  install-${CHART_NAME}.sh: |
    #!/bin/bash
    set -ex
    mkdir -p \${HELM_HOME}
    cp /etc/helm/* \${HELM_HOME}

    helm repo add releases https://registry.cern.ch/chartrepo/releases
    helm repo update

    if [[ \$(helm history ${CHART_NAME} | grep ${CHART_NAME}) ]]; then
        echo "${CHART_NAME} already installed on server. Continue..."
        exit 0
    else
        helm -n kube-system install ${CHART_NAME} releases/${CHART_NAME} --wait --version ${CERN_CHART_VERSION} --values /opt/magnum/install-${CHART_NAME}-values.yaml
    fi

  install-${CHART_NAME}-values.yaml:  |
$(cat ${_cern_chart_values_file})

---

apiVersion: batch/v1
kind: Job
metadata:
  name: install-${CHART_NAME}-job
  namespace: magnum-tiller
spec:
  backoffLimit: 5
  template:
    spec:
      serviceAccountName: tiller
      containers:
      - name: config-helm
        image: ${CONTAINER_INFRA_PREFIX:-docker.io/openstackmagnum/}helm-client:v3.2.0
        command:
        - bash
        args:
        - /opt/magnum/install-${CHART_NAME}.sh
        env:
        - name: HELM_HOME
          value: /helm_home
        - name: TILLER_NAMESPACE
          value: magnum-tiller
        - name: HELM_TLS_ENABLE
          value: "true"
        volumeMounts:
        - name: install-${CHART_NAME}-config
          mountPath: /opt/magnum/
        - mountPath: /etc/helm
          name: helm-client-certs
      restartPolicy: Never
      volumes:
      - name: install-${CHART_NAME}-config
        configMap:
          name: ${CHART_NAME}-config
      - name: helm-client-certs
        secret:
          secretName: helm-client-secret
EOF
    }

fi

printf "Finished running ${step}\n"
